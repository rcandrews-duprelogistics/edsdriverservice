﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDSDriverService.Models
{
    public partial class DriverManifest
    {
        public string Sequence { get; set; }
        public string Ordernumber { get; set; }
    }
}
