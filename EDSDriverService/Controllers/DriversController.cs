﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EDSDriverService.Models;
using Microsoft.Extensions.Logging;
using Snickler.EFCore;
using Newtonsoft.Json;

namespace EDSRestService.Controllers
{
    [Produces("application/json")]
    [Route("api/Drivers")]
    public class DriversController : Controller
    {
         private readonly TMW_Context _context;
        private ILogger<DriversController> _logger;

        public DriversController(TMW_Context context, ILogger<DriversController> logger)
        {
            _context = context;
            _logger = logger;
        }
        [HttpGet("DriverSchedule/{driverID}")]

        public async Task<IActionResult> GetDriverManifest([FromRoute] string driverID)
        {
            var driverManifest = new List<DriverManifest>();
            CallDupreDriverOrders(driverManifest, driverID);
            if (driverManifest.Count > 0)
                return Ok(driverManifest);
            else
                return NotFound($"No orders found for DriverID: {driverID}");
        }
        private void CallDupreDriverOrders(List<DriverManifest> retManifestResult, string driverID)
        {
            _context.LoadStoredProc("dbo.DupreDriverOrders")
                     .WithSqlParam("Drivercode", driverID)
                     .ExecuteStoredProc((handler) =>
                     {
                         try
                         {
                             var sprocResults = handler.ReadToList<DriverManifest>();
                             foreach (DriverManifest dManifest in sprocResults)
                             {
                                 retManifestResult.Add(dManifest);
                             }
                         }
                         catch (Exception ex)
                         {
                             Console.WriteLine(ex.Message);
                         }
                     });
        }
    }
}